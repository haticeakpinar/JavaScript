function Person(firstName, lastName) {
    this.firstName = firstName
    this.lastName = lastName
}

Person.prototype.message = function (){
    return `Hello ${this.firstName} ${this.lastName}`
}

const person = new Person('alan' , 'smith')
console.log(person.message())

function Customer(firstName,lastName,phone,email){
    Person.call(this, firstName, lastName)
    this.phone = phone
    this.email = email
}
Customer.prototype = Object.create(Person.prototype);
Customer.prototype.constructor = Customer
const customer = new Customer('peter','william','123456','email@email.com')

console.log(customer.message())
console.log(customer)

const profilePrototype = {
    message: function (){
        return `Hello, I am ${this.firstName} ${this.lastName}`
    },
    setNewLastName: function (newLastName){
        this.lastName = newLastName
    }
}
const peter = Object.create(profilePrototype)
peter.firstName = 'Peter'
peter.lastName = 'William'
peter.age = 18
peter.setNewLastName('Doe')
console.log(peter.message())

const alan = Object.create(profilePrototype,{
    firstname : {value: 'alan'},
    lastname : {value : 'smith'},
    age : {value : '30'}
})
console.log(alan)
