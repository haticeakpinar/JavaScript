const Smith ={
    name : 'Smith',
    age : 40
}
// console.log(Smith)

function Person(name, dob){
    this.name = name
    this.dob = new Date(dob)
    this.calculate_age = function (){
        var diff_ms = Date.now()- this.dob.getTime();
        var age_dt = new Date(diff_ms);

        return Math.abs(age_dt.getUTCFullYear()-1970)
    }
    console.log(this.calculate_age())
}
const Peter = new Person('peter', '12-13-1986')
const Hatice = new Person('Hatice', '12-23-1996')

const name1 = "Smith";
const name2 = new String("smith");
name2.age = 18;
name2.lastname = "brown"
console.log(name1, typeof (name1))
console.log(name2, typeof (name2))

const num1 = 8;
const num2 = new Number(8);
console.log(num1)
console.log(num2)

let item1 = true
let item2 = new Boolean(true)

console.log(item1)
console.log(item2)

class Person2{
    constructor(firstName, lastName, dob) {
        this.firstName = firstName
        this.lastName = lastName
        this.dob = dob
    }
    message(){
        return `Hello I am ${this.firstName} ${this.lastName}`
    }
    calculate_age(){
        var diff_ms = Date.now()-this.dob.getTime();
        var age_dt = new Date(diff_ms);

        return Math.abs(age_dt.getUTCFullYear()-1970);

    }
    setNewLastName(newLastName){
       this.lastName = newLastName
    }
}

const alan = new Person2('alan', 'smith', '11-13-1989')
alan.setNewLastName('william')
console.log(alan)

class Person3{
    constructor(firstName, lastName, dob) {
        this.firstname = firstName
        this.lastName = lastName
        this.dob = dob
    }
    message(){
        return `Hello ${firstName} ${lastName}`
    }
}
class Customer extends Person3{
    constructor(firsName, lastName, phone, email) {
        super(firsName,lastName);
        this.phone = phone
        this.email = email
    }
}
const hatice = new Customer('hatice', 'akpınar', '152151','hatice@hatice.com')
console.log(hatice)





