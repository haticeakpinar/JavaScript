console.log('hello world');
var message = 'this is new message'
console.log(message);
var array = [1, 2, 3]
console.log(array);
console.log({
        name: 'hatice',
        age: 25
    }
)
console.table({
        name: 'hatice',
        age: 25
    }
)
console.error('my name is not hatice')
console.warn('warning!')
//console.clear()
console.time('time')
console.log('timing')
console.log('timing')
console.log('timing')
console.log('timing')
console.log('timing')
console.log('timing')
console.timeEnd('time')


const user = {
    name: 'Alan',
    age: '25'
}
console.log(user)
user.name = 'John'
console.log(user);

const arr = []
console.log(arr);
arr.push(3,4)
console.log(arr);

let item
item = 'hello'
let item2 = true;
item3 = null
let item4 = 5
let item5 = Symbol()
console.log(item3)
console.log('type', typeof item5)

let name = 'alan'
function messages(name) {
    name = "peter"
    console.log('name is in function', name)
}
messages(name)
console.log(name);

let val;
val = 999
val = String(val)

val = new Date()
val = val.toString()

console.log(val)
console.log(typeof val)
console.log(val.length)

val = [1,2,3,4]
val = Number(val)
val = parseInt(val)
console.log(val,typeof val)
val = '12.34'
val = parseFloat(val)
console.log(val)

const num = 8
const num2 = 6
let val2
val2 = num + num2
val2 = num - num2
val2 = num / num2
val2 = num * num2
val2 = num % num2
val2 = Math.max(2,6,8,10,3)
val2 = Math.min(2,6,8,10,3)
val2 = Math.abs(-4)
val2 = Math.random()
console.log(val2)

const firstName = 'Alan'
const lastName = 'Walker'
val3 = firstName+ ' ' +lastName
val3 = firstName.toUpperCase()
val3 = firstName.toLowerCase()
console.log(val3)

val4 = 'Alan'
val4 += 'Smith'
console.log(val4)

let firstname2 = 'Alan'
let lastname2 = 'Walker'
let age2 = 30
let drinking = 'beer'
if (age2>18){
    drinking = 'beer'
}
else{
    drinking = 'milk'
}
message2 = `hello my name is ${firstname2}. I am ${age2} years old. I love drinking ${drinking}`
console.log(message2)

let vall
const d = new Date()
vall = d
console.log(vall.getFullYear())
console.log(vall.getMonth())
console.log(vall.getDate())

const mark = '50'

if(mark != 40){
    console.log('the mark is not 40')
} else{
    console.log('the mark is not 40')
}

if (typeof mark !== 'undefined'){
    console.log(`the mark is ${mark}`)
}else{
    console.log(`the mark is undefined`)
}

if(mark<=50){
  console.log('you failed the test')
}else{
    console.log('you pass the test')
}

let hour = 12;
let isWeekend = false

if(hour<10 || hour>17 || isWeekend){
    console.log('the office is not closed')
}else{
    console.log('the office is now opened.')
}

let age3 = 20
const drink = age3<18? 'you cannot drink beer':'you can drink beer'
console.log(drink)
console.log('hatice')

const fruit = "fghgfhg"
switch(fruit){
    case "apple":
        console.log('red')
        break;
    case "orange":
        console.log('orange')
        break;
    case "banana":
        console.log('yellow')

    default:
        console.log('I don\'t know')
        break;
}

const x = 8
switch (true) {
    case(x<5):
        console.log("less than 5")
        break;
    case (x<10):
        console.log("between 5 and 10")
        break;

    default:
        console.log('greater than')
        break;
}

function users(name, age){
    if (typeof  name == 'undefined'){
        name = 'John'
    }
    if (typeof age == 'undefined'){
        age = 20
    }
    return `hello, my name is ${name}. I am ${age} years old`
}
console.log(users('alan',18))

//IIFE (Immediately Invoked Function Expression)
var num3 =  90;
(function (){
    var num3 = 3;
    console.log(num3);
})()
console.log('outside IIFE', num3)

const person = {
    hello: function () {
        console.log('hello')
    },
    age: function (age){
        console.log(`i am ${age} years old`)
    }
}

person.laugh = function (){
    console.log('Ha ha')
}
person.hello()
person.age(18)
person.laugh()

for ( let i = 0; i<10 ; i++){
    if(i===3){
        console.log('Three')
        continue
    }

    if(i===6){
        console.log(i)
        break;
    }
    console.log(i)
}

//While loop
let i=0
while (i<10){
    console.log(i)
    i++;
}

//Do while
let i2=0
do{
    console.log(i2)
    i2++
}while(i2<10);

//loop through array
const people = ['alan', 'peter', 'john']

//for loop
for (let i=0; i<=users.length; i++){
    console.log(people[i])
}
//for of
for(const peop of people){
    console.log(people)
}
//For each
people.forEach(function (user, index,arr){
   console.log(index, user, arr)
});
//map
const hiUsers = people.map(function (peop){
    return `Hi, ${peop}`
});
console.log(hiUsers)

const name2 = prompt('What is your name?')
alert(`Hello, ${name2}`)

if (confirm('Are you sure to delete?')){
    alert('The file is deleted')
}
//outer width and height
let oh = window.outerHeight;
let ow = window.outerWidth;
console.log(oh, ow)
//inner width and height
let ih = window.innerHeight;
let iw = window.innerWidth;
console.log(ih, iw)
//scroll points
let sx = window.scrollX
let sy = window.scrollY
console.log(sx, sy)
//location object
let val5;
val5 = window.location;
val5 = window.location.hostname
val5 = window.location.port
val5 = window.navigator.appName
val5 = window.navigator.appVersion
val5 = window.navigator.userAgent
val5 = window.navigator.platform
val5 = window.navigator.vendor
val5 = window.navigator.language
console.log(val5)




